const express = require("express");
const app = express();
const cors = require("cors");
const mercadopago = require("mercadopago");
const { default: axios } = require("axios");
const Sentry = require("@sentry/node");



Sentry.init({
	dsn: "https://611cf2653f7340d9a780f28bb657d07b@o972761.ingest.sentry.io/6269206",
	tracesSampleRate: 1.0,
});


app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());
app.use(express.static("../../client"));

app.get("/", function (req, res) {
	res.status(200).json({ message: 'UP!' });
});

app.post("/create_preference", async (req, res) => {
	// REPLACE WITH YOUR ACCESS TOKEN AVAILABLE IN: https://developers.mercadopago.com/panel
	mercadopago.configure({
		access_token: req.body.access_token,
	});
	let items = []

	items = await getRealCartPrice(req.body.items)
	if (allValidItems(items)) {
		let preference = {
			items: req.body.items,
			back_urls: {
				"success": "https://e-commerce.devthion.com/checkout/thank-you",
				"failure": `https://e-commerce.devthion.com/${req.body.statement_descriptor}`
			},
			statement_descriptor: req.body.statement_descriptor,
			auto_return: "approved",
			binary_mode: true,
			payment_methods: {
				"excluded_payment_methods": [
					{
						"id": "pagofacil"
					},
					{
						"id": "rapipago"
					},
					{
						"id": "cargavirtual"
					},
					{
						"id": "cobroexpress"
					},
					{
						"id": "redlink"
					}
				]
			},
		};
		mercadopago.preferences.create(preference)
			.then(function (response) {
				res.json({
					id: response.body.id
				});
			}).catch(function (error) {
				res.status(500).json({
					message: 'MercadoPago esta inhabilitado, ya que el carrito tiene productos que no son validos o que ya no existen en la tienda'
				});
			});
	} else {
		res.status(500).json({
			message: 'MercadoPago esta inhabilitado, ya que el carrito tiene productos que no son validos o que ya no existen en la tienda'
		});
	}


});


const getRealItemPrice = async (item) => {
	let data
	try {
		data = await axios.get(`https://devthion-api.herokuapp.com/commerce/products/product/${item._id}`);
		data = data.data.price
	} catch (error) {
		data = null
	}
	return data
}

const getRealCartPrice = async (cartItems) => {
	await Promise.all(cartItems.map(async (item) => {
		item.unit_price = await getRealItemPrice(item)
	}))
	return cartItems
}

const allValidItems = (items) => {
	let valid = true
	items.forEach((item) => {
		if (!item.unit_price) {
			valid = false
		}
	})

	return valid
}

module.exports = app